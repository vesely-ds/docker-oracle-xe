#!/bin/bash

# Připravit potřebné soubory a adresáře pro instalaci
mv chkconfig /sbin
chmod 755 /sbin/chkconfig
ln -s /usr/bin/awk /bin/awk
mkdir /var/lock/subsys

# Sloučit části RMP balíčku dohromady, zkonvertovat na DEB balíček a nainstalovat
cat oracle-xe-11.2.0-1.0.x86_64.rpm.* > oracle-xe-11.2.0-1.0.x86_64.rpm
alien --scripts -d oracle-xe-11.2.0-1.0.x86_64.rpm
dpkg --install oracle-xe_11.2.0-2_amd64.deb

# Spusti konfiguraci Oracle
sed -i 's/^\(memory_target=.*\)$/#\1/' /u01/app/oracle/product/11.2.0/xe/config/scripts/init.ora && \
sed -i 's/^\(memory_target=.*\)$/#\1/' /u01/app/oracle/product/11.2.0/xe/config/scripts/initXETemp.ora && \
/etc/init.d/oracle-xe configure responseFile=xe.rsp

# Úklid
rm -f *
