#!/bin/bash

# Opravit konfiguraci Oracle Net Listener
sed -i -E "s/HOST = [^)]+/HOST = ${HOSTNAME}/g" /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora

# Nastartovat Oracle
service oracle-xe start
echo "Oracle started successfully!"

# Nekonečná smyčka, aby nebyl Docker kotejner zastaven
while true; do sleep 3600; done
