FROM ubuntu:trusty

# Nainstalovat potřebné balíčky
RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y alien bc libaio1 && \
rm -rf /var/lib/apt/lists/*

# Nainstalovat Oracle
COPY assets/setup /tmp
WORKDIR /tmp
RUN /bin/bash -e setup.sh

# Připravit startovací skript
COPY assets/start.sh /
RUN chmod 755 /start.sh
CMD /bin/bash -e /start.sh

# Porty a svazky
EXPOSE 1521 8080
VOLUME /usr/lib/oracle/xe/oradata/XE
